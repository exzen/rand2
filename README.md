rand (XXX renamed to rand2 for exzen)
====

A Rust library for random number generators and other randomness functionality.

[![Build Status](https://travis-ci.org/rust-lang-nursery/rand.svg?branch=master)](https://travis-ci.org/rust-lang-nursery/rand)
[![Build status](https://ci.appveyor.com/api/projects/status/rm5c9o33k3jhchbw?svg=true)](https://ci.appveyor.com/project/alexcrichton/rand)

[Documentation](https://doc.rust-lang.org/rand)

## Usage

DO NOT Add this to your `Cargo.toml`:

```toml
[dependencies]
rand = "0.3"

INSTEAD use

```toml
[dependencies]
rand = { git = "git@bitbucket.org:exzen/rand2.git" }
```

and this to your crate root:

```rust
extern crate rand2;
```
